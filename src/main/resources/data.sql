INSERT INTO "farmacia" 
    (titulo,creado)
SELECT 'El dealer','10/12/2020'
WHERE
    NOT EXISTS (
        SELECT id FROM farmacia WHERE id = 1
    );

INSERT INTO "usuario"
 ("is_admin", "nombre", "password", "usuario")
 SELECT true,'EL ADMINISTRADOR NOMBRE','admin','admin'
WHERE
    NOT EXISTS (
        SELECT id FROM usuario WHERE id = 1
    );



INSERT INTO "producto" ( "nombre","precio")
SELECT 'intestonomicina', 1.50
WHERE
    NOT EXISTS (
        SELECT id FROM producto WHERE id = 1
    );

INSERT INTO "producto" ( "nombre","precio")
SELECT 'froskrol', 15.20
WHERE
    NOT EXISTS (
        SELECT id FROM producto WHERE id = 2
    );

INSERT INTO "producto" ( "nombre","precio")
SELECT 'paracetamol', 0.15
WHERE
    NOT EXISTS (
        SELECT id FROM producto WHERE id = 3
    );
