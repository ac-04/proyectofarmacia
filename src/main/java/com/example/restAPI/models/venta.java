package com.example.restAPI.models;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.data.annotation.CreatedDate;

@Entity
@Table(name = "ventas")
public class venta {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "idUsuario")
	private Long idUsuario;
	@CreatedDate
	private Date createdAt;
	
	
	 @JoinTable(
		        name = "rel_venta_producto",
		        joinColumns = @JoinColumn(name = "idventa", nullable = false),
		        inverseJoinColumns = @JoinColumn(name="idproducto", nullable = false)
		    )
	 
		    @ManyToMany(cascade =  {
	                CascadeType.DETACH,
	                CascadeType.MERGE,
	                CascadeType.REFRESH,
	                CascadeType.PERSIST
	        })
		    private List<producto> producto;
		    

	public List<producto> getProducto() {
		return producto;
	}
	public void setProducto(List<producto> producto) {
		this.producto = producto;
	}
	
	public void addProducto(producto productoIn){
        if(this.producto == null){
            this.producto = new ArrayList<>();
        }
        
        this.producto.add(productoIn);
    }
	@Transient
	private String nombre;
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(Long idUsuario) {
		this.idUsuario = idUsuario;
	}
	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	
	
	
}
