package com.example.restAPI.routers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.restAPI.DAO.usuariosDAO;
import com.example.restAPI.models.producto;
import com.example.restAPI.models.usuario;

@CrossOrigin(maxAge = 3600) // ESTO PERMITE PODER TOMAR LOS PREFLIGTHS DE EL METODO OPTION, A LA HORA DE HACER UN POST
//Problemas que da cuando se intenta hacer un post desde una app front_end con axios, HttpXMLRequest Ajax.
//https://spring.io/blog/2015/06/08/cors-support-in-spring-framework
@RestController
@RequestMapping("login")
public class loginROUTER {
	
	@Autowired
	usuariosDAO usuariodao;
	 @RequestMapping(value = "",method = {RequestMethod.OPTIONS,RequestMethod.POST})
	 public ResponseEntity<usuario> insertProducto(@RequestBody usuario user){
		 usuario usuarioFind = usuariodao.findByusuario(user.getUsuario());
		 if (usuarioFind.getPassword().equals(user.getPassword())) {

			 return ResponseEntity.ok(usuarioFind);
		}else {
			return ResponseEntity.notFound().build();
		}
	 }

}
