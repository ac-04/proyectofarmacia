package com.example.restAPI.routers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.restAPI.DAO.productosDAO;
import com.example.restAPI.DAO.usuariosDAO;
import com.example.restAPI.DAO.venta_productosDAO;
import com.example.restAPI.DAO.ventasDAO;
import com.example.restAPI.models.producto;
import com.example.restAPI.models.usuario;
import com.example.restAPI.models.venta;
import com.example.restAPI.models.venta_producto;
import com.fasterxml.jackson.databind.ObjectMapper;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("ventas")
public class ventasROUTER {

	 @Autowired
	private ventasDAO ventasdao;
	 @Autowired
	 private usuariosDAO usuariosdao;
	 @Autowired
	 private venta_productosDAO ventaproductodao;
	 @Autowired
	 private productosDAO productodao;
	
	@GetMapping(value = "/user/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public void allventas(HttpServletResponse response, @PathVariable("id") Long id ) throws IOException{
		/**
		 * ALLOWING CORS
		 * */
		
		 response.setHeader("Access-Control-Allow-Origin","*");
	     response.setHeader("Access-Control-Allow-Methods", "POST, GET");
	     response.setHeader("Access-Control-Max-Age", "3600");
	     response.setHeader("Access-Control-Allow-Headers", "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
	     //se agrego Sort.By para poder ver los ultimos ventas agregados
	     List<venta> ventas = new ArrayList<venta>();
	     if (id==1) {
	 		ventas = ventasdao.findAll(); // Esta funcion se encuentra en la interfaz DAO
		}else {
			
			ventas = ventasdao.findByidUsuario(id); // Esta funcion se encuentra en la interfaz DAO
		}
        ObjectMapper mapper = new ObjectMapper(); // libreria jackson
	   if (!ventas.isEmpty()) {
		   
		   ventas.forEach(venta->{
			  Optional<usuario> user =usuariosdao.findById(venta.getIdUsuario());
			  venta.setNombre(user.get().getName());
			   });
		   
		    response.setStatus(200);
	        String json = mapper.writeValueAsString(ventas); // libreria jackson
	        response.setContentType("application/json"); // esto es lo que hace que se retorne un json
		    response.getWriter().println(json);
	   }else {
		 JSONObject json = new JSONObject();
	     JSONObject subJson = new JSONObject();
	    subJson .put("Message", "Err can no find any venta");
	    json.put("msg", subJson);
	    response.setStatus(404);
        response.setContentType("application/json"); // esto es lo que hace que se retorne un json
	    response.getWriter().println(json);
	    
	   }
	   
	}
	
	
	@GetMapping("{id}")
	public ResponseEntity<List<producto>> getProductosVenta(@PathVariable Long id){
		
		List<venta_producto> vp = ventaproductodao.findByidventa(id);
		List<producto> productosVenta =  new ArrayList<producto>();
		
		for (venta_producto ven : vp) {
			Optional<producto> optionalProducto = productodao.findById(ven.getIdproducto());
			producto prod = optionalProducto.get();
			prod.setCantidad(ven.getCantidad());
			productosVenta.add(prod);
		}
		return ResponseEntity.ok(productosVenta);
		
	}
	
	@RequestMapping(value = "{idVendedor}",method = {RequestMethod.OPTIONS,RequestMethod.POST})
	 public ResponseEntity<?> insertProducto(@RequestBody List<producto> products,@PathVariable Long idVendedor){
		venta ventaNueva = new venta();
		Date date = new Date();
		ventaNueva.setCreatedAt(date);
		ventaNueva.setIdUsuario(idVendedor);
		venta ventaRealizada = ventasdao.save(ventaNueva);
		for (producto producto : products) {
			venta_producto ventaProductos= new venta_producto();
			ventaProductos.setIdproducto(producto.getId());
			ventaProductos.setIdventa(ventaRealizada.getId());
			ventaProductos.setCantidad(producto.getCantidad());
			ventaproductodao.save(ventaProductos);
		}
		return ResponseEntity.ok(null);
	 }
	
	 @RequestMapping(value = "{id}",method = {RequestMethod.OPTIONS,RequestMethod.DELETE})
	 public ResponseEntity<?> deleteUser( @PathVariable("id") Long idVenta ){
		 if (idVenta!=1) {
			 ventasdao.deleteById(idVenta);
		}
		 return ResponseEntity.ok(null);
	 }
	 
	 @RequestMapping(value = "hello",method = RequestMethod.GET)
		public String hello() {
			return  "hello from productos";
		}
	
}
