package com.example.restAPI.routers;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.restAPI.DAO.usuariosDAO;
import com.example.restAPI.models.usuario;
import com.fasterxml.jackson.databind.ObjectMapper;
@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("usuarios")
public class usuariosROUTER {

	
	@Autowired
	usuariosDAO usuariosdao;
	
	
	@GetMapping(value = "",produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public void allUsuarios(HttpServletResponse response) throws IOException{
		/**
		 * ALLOWING CORS
		 * */
		
		 response.setHeader("Access-Control-Allow-Origin","*");
	     response.setHeader("Access-Control-Allow-Methods", "POST, GET");
	     response.setHeader("Access-Control-Max-Age", "3600");
	     response.setHeader("Access-Control-Allow-Headers", "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
	     //se agrego Sort.By para poder ver los ultimos usuarios agregados
		List<usuario> usuarios = usuariosdao.findAll(Sort.by(Sort.Direction.DESC,"id")); // Esta funcion se encuentra en la interfaz DAO
        ObjectMapper mapper = new ObjectMapper(); // libreria jackson
	   if (!usuarios.isEmpty()) {
		   
		    response.setStatus(200);
		    usuarios.forEach(user->{
		    	user.setPassword("no");
		    });
	        String json = mapper.writeValueAsString(usuarios); // libreria jackson
	        response.setContentType("application/json"); // esto es lo que hace que se retorne un json
		    response.getWriter().println(json);
	   }else {
		 JSONObject json = new JSONObject();
	     JSONObject subJson = new JSONObject();
	    subJson .put("Message", "Err can no find any user");
	    json.put("msg", subJson);
	    response.setStatus(404);
        response.setContentType("application/json"); // esto es lo que hace que se retorne un json
	    response.getWriter().println(json);
	    
	   }
	   
	}
	
	
	@RequestMapping(value = "",method = {RequestMethod.OPTIONS,RequestMethod.POST})
	 public ResponseEntity<usuario> insertUsuario(@RequestBody usuario user){
		 user.setUsuario(user.getUsuario().trim());
		 user.setPassword(user.getUsuario().trim());
		 usuario usuarioSaved = usuariosdao.save(user);
		 return ResponseEntity.ok(usuarioSaved);
	 }
	 
	 @RequestMapping(value = "",method = {RequestMethod.OPTIONS,RequestMethod.PUT})
	 public ResponseEntity<usuario> updateusuario(@RequestBody usuario updateUser){
		 Optional<usuario> optionalUser = usuariosdao.findById(updateUser.getId());
		 if (optionalUser.isPresent()) {
			usuario updatingUser = optionalUser.get();
			if (!updatingUser.getId().equals(1L)) { // Si no es el maestro no se actualizara
				updatingUser.setAdmin(updateUser.isAdmin());
				if (updatingUser.getPassword() == "cambiar") {
					updatingUser.setPassword(updatingUser.getUsuario()); // si desea resetear la contraseña sear el mismo el usuario
				}
				usuario updatedusuario = usuariosdao.save(updatingUser);
				updatedusuario.setPassword("no");
				return ResponseEntity.ok(updatedusuario);
			}else {
				updatingUser.setPassword("no");
				return ResponseEntity.ok(updatingUser);
			}
		}else {
			return ResponseEntity.noContent().build();
		}
	 }
	 
	 @RequestMapping(value = "unico",method = {RequestMethod.OPTIONS,RequestMethod.PUT})
	 public ResponseEntity<usuario> updateusuarioUnico(@RequestBody usuario updateUser){
		 Optional<usuario> optionalUser = usuariosdao.findById(updateUser.getId());
		 if (optionalUser.isPresent()) {
			usuario updatingUser = optionalUser.get();
			updatingUser.setName(updateUser.getName());
			updatingUser.setUsuario(updateUser.getUsuario());
			updatingUser.setPassword(updateUser.getPassword());// si desea resetear la contraseña sear el mismo el usuario
				usuario updatedusuario = usuariosdao.save(updatingUser);
				return ResponseEntity.ok(updatedusuario);
	 }else {
		 return ResponseEntity.noContent().build();
	 }
	 }
	
	 
	 @RequestMapping(value = "{id}",method = {RequestMethod.OPTIONS,RequestMethod.DELETE})
	 public ResponseEntity<?> deleteUser( @PathVariable("id") Long usuarioId ){
		 if (usuarioId!=1) {
			 usuariosdao.deleteById(usuarioId);
		}
		 return ResponseEntity.ok(null);
	 }
	 
	 @RequestMapping(value = "hello",method = RequestMethod.GET)
		public String hello() {
			return  "hello from productos";
		}
}
