package com.example.restAPI.routers;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.restAPI.DAO.productosDAO;
import com.example.restAPI.models.producto;
import com.fasterxml.jackson.databind.ObjectMapper;

@CrossOrigin(maxAge = 3600) // ESTO PERMITE PODER TOMAR LOS PREFLIGTHS DE EL METODO OPTION, A LA HORA DE HACER UN POST
// Problemas que da cuando se intenta hacer un post desde una app front_end con axios, HttpXMLRequest Ajax.
//https://spring.io/blog/2015/06/08/cors-support-in-spring-framework
@RestController
@RequestMapping("productos")
public class productosROUTER {

	 @Autowired
	private productosDAO productosdao;
	
	/*@GetMapping
	public ResponseEntity<List<producto>> all(){
		return ResponseEntity.ok(productosdao.findAll());
	}*/
	 
	
	@GetMapping(value = "",produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public void allProductos(HttpServletResponse response) throws IOException{
		/**
		 * ALLOWING CORS
		 * */
		
		 response.setHeader("Access-Control-Allow-Origin","*");
	     response.setHeader("Access-Control-Allow-Methods", "POST, GET");
	     response.setHeader("Access-Control-Max-Age", "3600");
	     response.setHeader("Access-Control-Allow-Headers", "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
	     //se agrego Sort.By para poder ver los ultimos productos agregados
		List<producto> productos = productosdao.findAll(Sort.by(Sort.Direction.DESC,"id")); // Esta funcion se encuentra en la interfaz DAO
        ObjectMapper mapper = new ObjectMapper(); // libreria jackson
	   if (!productos.isEmpty()) {
		   
		    response.setStatus(200);
	        String json = mapper.writeValueAsString(productos); // libreria jackson
	        response.setContentType("application/json"); // esto es lo que hace que se retorne un json
		    response.getWriter().println(json);
	   }else {
		 JSONObject json = new JSONObject();
	     JSONObject subJson = new JSONObject();
	    subJson .put("Message", "Err can no find any product");
	    json.put("msg", subJson);
	    response.setStatus(404);
        response.setContentType("application/json"); // esto es lo que hace que se retorne un json
	    response.getWriter().println(json);
	    
	   }
	   
	}
	 @RequestMapping(value = "",method = {RequestMethod.OPTIONS,RequestMethod.POST})
	 public ResponseEntity<producto> insertProducto(@RequestBody producto product){
		 producto productoSaved = productosdao.save(product);
		 return ResponseEntity.ok(productoSaved);
	 }
	 
	 @RequestMapping(value = "",method = {RequestMethod.OPTIONS,RequestMethod.PUT})
	 public ResponseEntity<producto> updateProducto(@RequestBody producto updateProduct){
		 Optional<producto> optionalProduct = productosdao.findById(updateProduct.getId());
		 if (optionalProduct.isPresent()) {
			producto updatingProduct = optionalProduct.get();
			updatingProduct.setDescripcion(updateProduct.getDescripcion());
			updatingProduct.setName(updateProduct.getName());
			updatingProduct.setPrecio(updateProduct.getPrecio());
			producto updatedProducto = productosdao.save(updatingProduct);
			return ResponseEntity.ok(updatedProducto);
		}else {
			return ResponseEntity.noContent().build();
		}
	 }
	 
	 @RequestMapping(value = "{id}",method = {RequestMethod.OPTIONS,RequestMethod.DELETE})
	 public ResponseEntity<?> deleteProduct( @PathVariable("id") Long productoId ){
		 productosdao.deleteById(productoId);
		 return ResponseEntity.ok(null);
	 }
	 
	 
	@RequestMapping(value = "hello",method = RequestMethod.GET)
	public String hello() {
		return  "hello from productos";
	}
	
}
