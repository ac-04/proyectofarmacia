package com.example.restAPI.routers;

import com.example.restAPI.DAO.farmaciaDAO;
import com.example.restAPI.models.farmacia;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/farmacia")
public class farmaciaROUTER {
    @Autowired
    private farmaciaDAO farmaciad;
    @GetMapping("all")
    public ResponseEntity<List<farmacia>> getInfo(){
    	return ResponseEntity.ok(farmaciad.findAll());
    }
    
    @GetMapping("/hello")
    public String hello(){
        return "hola";
    }

}
