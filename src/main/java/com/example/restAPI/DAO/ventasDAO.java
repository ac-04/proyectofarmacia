package com.example.restAPI.DAO;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.restAPI.models.venta;
import com.example.restAPI.models.venta_producto;

@Repository
public interface ventasDAO extends JpaRepository<venta, Long>{
	List<venta> findByidUsuario(Long id);

}
