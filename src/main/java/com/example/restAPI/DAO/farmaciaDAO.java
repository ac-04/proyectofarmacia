package com.example.restAPI.DAO;

import com.example.restAPI.models.farmacia;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface farmaciaDAO extends JpaRepository<farmacia,Long> {
}
