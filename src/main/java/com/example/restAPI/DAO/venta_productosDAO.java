package com.example.restAPI.DAO;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.restAPI.models.venta_producto;

@Repository
public interface venta_productosDAO extends JpaRepository<venta_producto, Long>{

	List<venta_producto> findByidventa(Long id);
}
