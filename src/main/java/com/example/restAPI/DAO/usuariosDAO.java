package com.example.restAPI.DAO;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.restAPI.models.usuario;

@Repository
public interface usuariosDAO extends JpaRepository<usuario,Long>{

	usuario findByusuario(String usuario);
	
}
