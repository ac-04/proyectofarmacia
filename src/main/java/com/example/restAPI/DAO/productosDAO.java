package com.example.restAPI.DAO;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.restAPI.models.producto;
@Repository
public interface productosDAO extends JpaRepository<producto,Long> {
	public List<producto> findAllByOrderByIdAsc(); //esta es por no menclatura siguiendo camelCase 
	/**
	 * */
	

}
